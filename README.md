# ECLAP - Junta de Castilla y León
# Curso de Integración Continua
# Ejercicio final

Proyecto generado con la herramienta [Spring Boot](https://start.spring.io/).

### Características
- Spring Boot 2.4.5
- Gradle Project
- Java 8
- War
- Spring Boot DevTools
- Spring Web